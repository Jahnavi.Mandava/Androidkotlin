import java.io.*
import java.util.*
import java.text.*
import java.math.*
import java.util.regex.*
object Solution {
    private val minComparator = object:Comparator<Int> {
        public override fun compare(first:Int, second:Int):Int {
            return first.compareTo(second)
        }
    }
    private val maxComparator = object:Comparator<Int> {
        public override fun compare(first:Int, second:Int):Int {
            return second.compareTo(first)
        }
    }
    private val minSortedQueue = PriorityQueue<Int>(100, minComparator)
    private val maxSortedQueue = PriorityQueue<Int>(100, maxComparator)
    @JvmStatic fun main(args:Array<String>) {
        val `in` = Scanner(System.`in`)
        val foundValues:ArrayList<Int>
        val n = `in`.nextInt()
        for (currentIndex in 1..n)
        {
            val value = Integer.valueOf(`in`.nextInt())
            var median = -1.0
            // alternate insert to keep tree balanced
            if (currentIndex % 2 == 0)
            {
                maxSortedQueue.add(value)
            }
            else
            {
                minSortedQueue.add(value)
            }
            if (minSortedQueue.size > 0 && maxSortedQueue.size > 0)
            {
                if (minSortedQueue.peek() < maxSortedQueue.peek())
                {
                    val maxSortedValue = maxSortedQueue.poll()
                    val minSortedValue = minSortedQueue.poll()
                    minSortedQueue.add(maxSortedValue)
                    maxSortedQueue.add(minSortedValue)
                }
            }
            if (currentIndex == 1)
            {
                median = value.toDouble()
            }
            else if (currentIndex % 2 != 0)
            {
                median = minSortedQueue.peek().toDouble()
            }
            else
            {
                val sum = (minSortedQueue.peek() + maxSortedQueue.peek()).toDouble()
                median = sum / 2.0
            }
            println(" "+ median)
        }
    }
}



